import React from "react";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  FormGroup,
  Form,
  Table,
  Input,
  Row,
  Col,
  CardFooter,
  Modal,
  ModalHeader,
  ModalBody,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  ModalFooter,
} from "reactstrap";
import NotificationAlert from "react-notification-alert";

export default class FosterCares extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      donors: [],
      fosterCares: [],
      modal: false,
      donor: "",
      start_date: 0,
      end_date: 0,
      description: [],
      cash_amount: 0,
      donor_id: -1,
    };
    this.notify = this.notify.bind(this);
    this.onDismiss = this.onDismiss.bind(this);
  }
  addNewFosterCare(e) {
    e.preventDefault();
    this.setState({
      modal: true,
    });
  }
  async getDonor(id) {
    const resopnse = await fetch(`http://localhost:8001/api/donor/${id}`);
    const json = await resopnse.json();
    if (json.success === true) {
      this.setState({
        donor: json.donor,
      });
    }
    return this.state.donor;
  }
  notify(type, message, action) {
    var options = {};
    options = {
      place: "tc",
      message: (
        <div>
          <div>
            {message}<b>Foster Care</b> was {action}
          </div>
        </div>
      ),
      type: type,
      icon: "now-ui-icons ui-1_bell-53",
      autoDismiss: 7,
    };
    this.refs.notificationAlert.notificationAlert(options);
  }
  onDismiss() {}
  handleTextEdit = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  async handleModalNewFosterCare(e) {
    e.preventDefault();
    const body = new FormData();
    body.append("orphan_nb", this.props.orphan_nb);
    body.append("donor_id", this.state.donor_id);
    body.append("start_date", this.state.start_date);
    body.append("end_date", this.state.end_date);
    body.append("cash_amount", this.state.cash_amount);

    body.append("description", this.state.description);

    // debugger;
    const response = await fetch(`http://localhost:8001/api/addNewFosterCare`, {
      method: "POST",
      body,
    });
    const result = await response.json();
    if (result.success) {
      this.setState({
        modal: false,
        donor_id: 0,
        start_date: 0,
        description: "",
        end_date: 0,
        cash_amount: 0,
        update: false,
        donor: [],
        fosterCare_id: -1
      });
      this.notify("info", "New", "added");
      this.componentDidMount();
    }
  }
  async componentDidMount() {
    const response = await fetch(
      `http://localhost:8001/api/fosterCare/${this.props.orphan_nb}`
    );
    const json = await response.json();
    if (json.success === true) {
      this.setState({
        fosterCares: json.fosterCares,
      });
    }
    const resopnse2 = await fetch(`http://localhost:8001/api/donors`);
    const json2 = await resopnse2.json();
    if (json2.success === true) {
      this.setState({
        donors: json2["donors"],
      });
    }
  }
  updateFosterCare(f) {
    this.getDonor(f.donor_id);
    this.setState({
      start_date: f.start_date,
      end_date: f.end_date,
      donor_id: f.donor_id,
      description: f.description,
      cash_amount: f.cash_amount,
      update: true,
      fosterCare_id: f.id
    });
  }
  async handleModalUpdateFosterCare (e) {
    e.preventDefault();
    const body = new FormData();
    body.append("orphan_nb", this.props.orphan_nb);
    body.append("donor_id", this.state.donor_id);
    body.append("start_date", this.state.start_date);
    body.append("end_date", this.state.end_date);
    body.append("cash_amount", this.state.cash_amount);
    body.append("description", this.state.description);
    // debugger;
    const response = await fetch(`http://localhost:8001/api/editFosterCare/${this.state.fosterCare_id}`, {
      method: "POST",
      body,
    });
    const result = await response.json();
    if (result.success) {
      this.setState({
        donor_id: 0,
        start_date: 0,
        description: "",
        end_date: 0,
        donor: [],
        cash_amount: 0,
        update: false,
      });
      this.notify("info","", "updated");
      this.componentDidMount();
    }
  }
  render() {
    return (
      <>
        <NotificationAlert ref="notificationAlert" />

        <Modal size="lg" isOpen={this.state.modal}>
          <ModalHeader>New Foster Care</ModalHeader>
          <ModalBody>
            <Form>
              <Row>
                <Col className="pr-1" md="6">
                  <FormGroup>
                    <label>Orphan Number</label>
                    <Input
                      defaultValue={this.props.orphan_nb}
                      disabled
                      type="text"
                      name="orphan_nb"
                    />
                  </FormGroup>
                </Col>
                <Col className="pr-1" md="6">
                  <FormGroup>
                    <label>Donor</label>
                    <Input
                      onChange={(e) =>
                        this.setState({
                          donor_id: e.target.value,
                        })
                      }
                      type="select"
                      name="selectMulti"
                    >
                      <option>Choose Donor</option>
                      {this.state.donors.map((d) => (
                        <option value={d.id} tag="a">
                          {d.name}
                        </option>
                      ))}
                    </Input>
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col className="pr-1" md="6">
                  <FormGroup>
                    <label>Start Date</label>
                    <Input
                      type="number"
                      name="start_date"
                      onChange={(e) => {
                        this.handleTextEdit(e);
                      }}
                    />
                  </FormGroup>
                </Col>
                <Col className="pr-1" md="6">
                  <FormGroup>
                    <label>End Date</label>
                    <Input
                      name="end_date"
                      type="number"
                      onChange={(e) => {
                        this.handleTextEdit(e);
                      }}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col className="pr-1" md="6">
                  <FormGroup>
                    <label>Description</label>
                    <Input
                      name="description"
                      type="text"
                      onChange={(e) => {
                        this.handleTextEdit(e);
                      }}
                    />
                  </FormGroup>
                </Col>
                <Col className="pr-1" md="6">
                  <FormGroup>
                    <label>Cash Amount</label>
                    <Input
                      name="cash_amount"
                      type="number"
                      onChange={(e) => {
                        this.handleTextEdit(e);
                      }}
                    />
                  </FormGroup>
                </Col>
              </Row>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button
              color="info"
              rounded
              className="btn btn-info btn-sm btn-round"
              onClick={(e) => {
                this.handleModalNewFosterCare(e);
              }}
            >
              Save
            </Button>
            <Button
              color="danger"
              rounded
              className="btn btn-danger btn-sm btn-round"
              onClick={(e) =>
                this.setState({
                  modal: false,
                })
              }
            >
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
        <Modal size="lg" isOpen={this.state.update}>
          <ModalHeader>New Foster Care</ModalHeader>
          <ModalBody>
            <Form>
              <Row>
                <Col className="pr-1" md="6">
                  <FormGroup>
                    <label>Orphan Number</label>
                    <Input
                      value={this.props.orphan_nb}
                      disabled
                      type="text"
                      name="orphan_nb"
                    />
                  </FormGroup>
                </Col>
                <Col className="pr-1" md="6">
                  <FormGroup>
                    <label>Donor</label>
                    <Input
                      onChange={(e) =>
                        this.setState({
                          donor_id: e.target.value,
                        })
                      }
                      type="select"
                      name="selectMulti"
                    >
                      {this.state.donors.map((d) =>
                        this.state.donor_id === d.id ? (
                          <option selected value={d.id} tag="a">
                            {d.name}
                          </option>
                        ) : (
                          <option value={d.id} tag="a">
                            {d.name}
                          </option>
                        )
                      )}
                    </Input>
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col className="pr-1" md="6">
                  <FormGroup>
                    <label>Start Date</label>
                    <Input
                      defaultValue={this.state.start_date}
                      type="number"
                      name="start_date"
                      onChange={(e) => {
                        this.handleTextEdit(e);
                      }}
                    />
                  </FormGroup>
                </Col>
                <Col className="pr-1" md="6">
                  <FormGroup>
                    <label>End Date</label>
                    <Input
                      defaultValue={this.state.end_date}
                      name="end_date"
                      type="number"
                      onChange={(e) => {
                        this.handleTextEdit(e);
                      }}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col className="pr-1" md="6">
                  <FormGroup>
                    <label>Description</label>
                    <Input
                      defaultValue={this.state.description}
                      name="description"
                      type="text"
                      onChange={(e) => {
                        this.handleTextEdit(e);
                      }}
                    />
                  </FormGroup>
                </Col>
                <Col className="pr-1" md="6">
                  <FormGroup>
                    <label>Cash Amount</label>
                    <Input
                      defaultValue={this.state.cash_amount}
                      name="cash_amount"
                      type="number"
                      onChange={(e) => {
                        this.handleTextEdit(e);
                      }}
                    />
                  </FormGroup>
                </Col>
              </Row>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button
              color="info"
              rounded
              className="btn btn-info btn-sm btn-round"
              onClick={(e) => {
                this.handleModalUpdateFosterCare(e);
              }}
            >
              Update
            </Button>
            <Button
              color="danger"
              rounded
              className="btn btn-danger btn-sm btn-round"
              onClick={(e) =>
                this.setState({
                  update: false,
                })
              }
            >
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
        <Row>
          <Col className="pr-1" md="12">
            <Card>
              <CardHeader>
                <CardTitle tag="h4">Foster Care</CardTitle>
              </CardHeader>
              <CardBody>
                <Table responsive>
                  <thead className="text-info">
                    <tr>
                      <th className="text-center">Donor</th>
                      <th className="text-center">Start Date</th>
                      <th className="text-center">End Date</th>
                      <th className="text-center">Description</th>
                      <th className="text-center">Cash Amount</th>
                      <th className="text-center">Edit</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.fosterCares.map((f) => (
                      this.state.donors.map(d =>
                        d.id === f.donor_id?
                        <tr>
                        <td className="text-center">
                         {d.name}
                        </td>
                        <td className="text-center">{f.start_date}</td>
                        <td className="text-center">{f.end_date}</td>
                        <td className="text-center">{f.description}</td>
                        <td className="text-center">{f.cash_amount}</td>
                        <td className="text-center">
                          <a
                            href="#"
                            onClick={(e) => {
                              e.preventDefault();
                              this.updateFosterCare(f);
                            }}
                          >
                            <i
                              className="now-ui-icons design-2_ruler-pencil"
                              aria-hidden="true"
                            ></i>
                          </a>
                        </td>
                      </tr>:null

                    )))}
                  </tbody>
                </Table>
              </CardBody>
              <CardFooter
                style={{
                  textAlign: "center",
                }}
              >
                <Button
                  color="info"
                  color="info"
                  className="btn btn-info btn-sm btn-round"
                  onClick={(e) => {
                    this.addNewFosterCare(e);
                  }}
                >
                  <i class="fa fa-plus" aria-hidden="true"></i> Add new
                  Foster-Care
                </Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </>
    );
  }
}
