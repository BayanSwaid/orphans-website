/*!

=========================================================
* Now UI Dashboard React - v1.4.0
=========================================================

* Product Page: https://www.creative-tim.com/product/now-ui-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/now-ui-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";

// reactstrap components
import {
  Row,
  Col,
  CardTitle,
  Card,
  CardHeader,
  Button,
  CardBody,
} from "reactstrap";
import { Link } from "react-router-dom";
// core components
import PanelHeader from "components/PanelHeader/PanelHeader.js";
import icons from "variables/icons";
class Icons extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orphans: [],
      stopped: [],
      active: [],
    };
  }
  async componentDidMount() {
    const response3 = await fetch(
      `http://localhost:8001/api/fosterCarePresentOrphans`
    );
    const json3 = await response3.json();
    if (json3.success === true) {
      this.setState({
        active: json3.presentOrphans,
      });
    }
    const response = await fetch(`http://localhost:8001/api/orphans`);
    const json = await response.json();
    if (json.success === true) {
      this.setState({
        orphans: json.orphans,
      });
    }
    const response2 = await fetch(`http://localhost:8001/api/stoppedOrphans`);
    const json2 = await response2.json();
    if (json2.success === true) {
      this.setState({
        stopped: json2.stoppedOrphans,
      });
    }
  }
  render() {
    return (
      <>
        <PanelHeader size="sm" />
        <div className="content">
          <Card>
            <CardHeader>
              <CardTitle>
                <h5 className="title">Stopped Orphans</h5>
              </CardTitle>
            </CardHeader>
            <CardBody>
              <Row>
                {this.state.orphans.map((o) =>
                  this.state.stopped.includes(o.orphan_nb) &&
                  !this.state.active.includes(o.orphan_nb) &&
                  new Date().getFullYear() - o.birth_date < 18 ? (
                    <Col md="4">
                      <Card className="card-user">
                        <div className="image">
                          <img alt="..." src={require("assets/img/bg4.jpg")} />
                        </div>
                        <CardBody>
                          <div className="author">
                            <Link to={`/admin/orphan-profile/${o.orphan_nb}`}>
                              <img
                                alt="orphan avatar"
                                className="avatar border-gray"
                                src={
                                  o.gender === "F" || o.gender === "f"
                                    ? require(`assets/img/girl.png`)
                                    : require(`assets/img/boy.png`)
                                }
                              />
                              <h5 className="title">{`${o.name} ${o.father_name} ${o.family_name}`}</h5>
                            </Link>
                            <p className="description">{`Orphan Number: ${o.orphan_nb}`}</p>
                          </div>
                          <p className="description text-center">{`Address: ${o.region} - ${o.address}`}</p>
                        </CardBody>
                        <hr />
                        <div className="button-container"></div>
                      </Card>
                    </Col>
                  ) : null
                )}
              </Row>
            </CardBody>
          </Card>
          {/* <Col md={12}>
            <Card>
              <CardHeader>
                <h5 className="title">100 Awesome Nucleo Icons</h5>
                <p className="category">
                  Handcrafted by our friends from{" "}
                  <a href="https://nucleoapp.com/?ref=1712">NucleoApp</a>
                </p>
              </CardHeader>
              <CardBody className="all-icons">
                <Row>
                  {icons.map((prop, key) => {
                    return (
                      <Col
                        lg={2}
                        md={3}
                        sm={4}
                        xs={6}
                        className="font-icon-list"
                        key={key}
                      >
                        <div className="font-icon-detail">
                          <i className={"now-ui-icons " + prop} />
                          <p>{prop}</p>
                        </div>
                      </Col>
                    );
                  })}
                </Row>
              </CardBody>
            </Card>
          </Col> */}
        </div>
      </>
    );
  }
}

export default Icons;
