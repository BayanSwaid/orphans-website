/*!

=========================================================
* Now UI Dashboard React - v1.4.0
=========================================================

* Product Page: https://www.creative-tim.com/product/now-ui-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/now-ui-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
// react plugin for creating notifications over the dashboard
import NotificationAlert from "react-notification-alert";

// reactstrap components
import {
  Alert,
  Card,
  CardTitle,
  CardBody,
  CardHeader,
  Row,
  Col,
  Button,
} from "reactstrap";
import {Link} from 'react-router-dom';
// core components
import PanelHeader from "components/PanelHeader/PanelHeader.js";

class Notifications extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orphans: [],
      cancelled: [],
      active: []
    };
  }
  async componentDidMount() {
    const response3 = await fetch(
      `http://localhost:8001/api/fosterCarePresentOrphans`
    );
    const json3 = await response3.json();
    if (json3.success === true) {
      this.setState({
        active: json3.presentOrphans,
      });
    }
    const response = await fetch(`http://localhost:8001/api/orphans`);
    const json = await response.json();
    if (json.success === true) {
      this.setState({
        orphans: json.orphans,
      });
    }
    const response2 = await fetch(`http://localhost:8001/api/stoppedOrphans`);
    const json2 = await response2.json();
    if (json2.success === true) {
      this.setState({
        cancelled: json2.stoppedOrphans,
      });
    }
  }
  //     visible: true,
  //   };
  //   this.onDismiss = this.onDismiss.bind(this);
  //   this.notify = this.notify.bind(this);
  // }
  // onDismiss() {}
  // notify(place) {
  //   var color = Math.floor(Math.random() * 5 + 1);
  //   var type;
  //   switch (color) {
  //     case 1:
  //       type = "primary";
  //       break;
  //     case 2:
  //       type = "success";
  //       break;
  //     case 3:
  //       type = "danger";
  //       break;
  //     case 4:
  //       type = "warning";
  //       break;
  //     case 5:
  //       type = "info";
  //       break;
  //     default:
  //       break;
  //   }
  //   var options = {};
  //   options = {
  //     place: place,
  //     message: (
  //       <div>
  //         <div>
  //           Welcome to <b>Now UI Dashboard React</b> - a beautiful freebie for
  //           every web developer.
  //         </div>
  //       </div>
  //     ),
  //     type: type,
  //     icon: "now-ui-icons ui-1_bell-53",
  //     autoDismiss: 7,
  //   };
  //   this.refs.notificationAlert.notificationAlert(options);
  // }
  render() {
    return (
      <>
        
          <PanelHeader size="sm" />
          <div className="content">
          <Card>
                <CardHeader>
                  <CardTitle><h5 className="title">Cancelled Orphans</h5></CardTitle>
                </CardHeader>
                <CardBody>
          <Row>
            {this.state.orphans.map((o) =>
              this.state.cancelled.includes(o.orphan_nb) &&
              !this.state.active.includes(o.orphan_nb) &&
              new Date().getFullYear() - o.birth_date >= 18 ? (
                <Col md="4">
                  <Card className="card-user">
                    <div className="image">
                      <img alt="..." src={require("assets/img/bg4.jpg")} />
                    </div>
                    <CardBody>
                      <div className="author">
                        <Link to={`/admin/orphan-profile/${o.orphan_nb}`}>
                          <img
                            alt="orphan avatar"
                            className="avatar border-gray"
                            src={
                              o.gender === "F" || o.gender === "f"
                                ? require(`assets/img/girl.png`)
                                : require(`assets/img/boy.png`)
                            }
                          />
                          <h5 className="title">{`${o.name} ${o.father_name} ${o.family_name}`}</h5>
                        </Link>
                        <p className="description">{`Orphan Number: ${o.orphan_nb}`}</p>
                      </div>
                      <p className="description text-center">{`Address: ${o.region} - ${o.address}`}</p>
                    </CardBody>
                    <hr />
                    <div className="button-container"></div>
                  </Card>
                </Col>
              ) : null
            )}
          </Row>
          </CardBody></Card>
        </div>
        {/* <NotificationAlert ref="notificationAlert" /> */}
        {/* <Row>
            <Col md={6} xs={12}>
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Notifications Style</CardTitle>
                </CardHeader>
                <CardBody>
                  <Alert color="info">
                    <span>This is a plain notification</span>
                  </Alert>
                  <Alert
                    color="info"
                    isOpen={this.state.visible}
                    toggle={this.onDismiss}
                  >
                    <span>This is a notification with close button.</span>
                  </Alert>
                  <Alert
                    color="info"
                    className="alert-with-icon"
                    isOpen={this.state.visible}
                    toggle={this.onDismiss}
                  >
                    <span
                      data-notify="icon"
                      className="now-ui-icons ui-1_bell-53"
                    />
                    <span data-notify="message">
                      This is a notification with close button and icon.
                    </span>
                  </Alert>
                  <Alert
                    color="info"
                    className="alert-with-icon"
                    isOpen={this.state.visible}
                    toggle={this.onDismiss}
                  >
                    <span
                      data-notify="icon"
                      className="now-ui-icons ui-1_bell-53"
                    />
                    <span data-notify="message">
                      This is a notification with close button and icon and have
                      many lines. You can see that the icon and the close button
                      are always vertically aligned. This is a beautiful
                      notification. So you don't have to worry about the style.
                    </span>
                  </Alert>
                </CardBody>
              </Card>
            </Col>
            <Col md={6} xs={12}>
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Notification states</CardTitle>
                </CardHeader>
                <CardBody>
                  <Alert
                    color="primary"
                    isOpen={this.state.visible}
                    toggle={this.onDismiss}
                  >
                    <span>
                      <b> Primary - </b> This is a regular notification made
                      with color="primary"
                    </span>
                  </Alert>
                  <Alert
                    color="info"
                    isOpen={this.state.visible}
                    toggle={this.onDismiss}
                  >
                    <span>
                      <b> Info - </b> This is a regular notification made with
                      color="info"
                    </span>
                  </Alert>
                  <Alert
                    color="success"
                    isOpen={this.state.visible}
                    toggle={this.onDismiss}
                  >
                    <span>
                      <b> Success - </b> This is a regular notification made
                      with color="success"
                    </span>
                  </Alert>
                  <Alert
                    color="warning"
                    isOpen={this.state.visible}
                    toggle={this.onDismiss}
                  >
                    <span>
                      <b> Warning - </b> This is a regular notification made
                      with color="warning"
                    </span>
                  </Alert>
                  <Alert
                    color="danger"
                    isOpen={this.state.visible}
                    toggle={this.onDismiss}
                  >
                    <span>
                      <b> Danger - </b> This is a regular notification made with
                      color="danger"
                    </span>
                  </Alert>
                </CardBody>
              </Card>
            </Col>
            <Col md={12} xs={12}>
              <Card>
                <CardBody>
                  <div className="places-buttons">
                    <Row>
                      <Col md={6} className="ml-auto mr-auto text-center">
                        <CardTitle tag="h4">
                          Notifications Places
                          <p className="category">
                            Click to view notifications
                          </p>
                        </CardTitle>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg={8} xs={12} className="ml-auto mr-auto">
                        <Row>
                          <Col md={4} xs={12}>
                            <Button
                              color="primary"
                              block
                              onClick={() => this.notify("tl")}
                            >
                              Top Left
                            </Button>
                          </Col>
                          <Col md={4} xs={12}>
                            <Button
                              color="primary"
                              block
                              onClick={() => this.notify("tc")}
                            >
                              Top Center
                            </Button>
                          </Col>
                          <Col md={4} xs={12}>
                            <Button
                              color="primary"
                              block
                              onClick={() => this.notify("tr")}
                            >
                              Top Right
                            </Button>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg={8} xs={12} className="ml-auto mr-auto">
                        <Row>
                          <Col md={4} xs={12}>
                            <Button
                              color="primary"
                              block
                              onClick={() => this.notify("bl")}
                            >
                              Bottom Left
                            </Button>
                          </Col>
                          <Col md={4} xs={12}>
                            <Button
                              color="primary"
                              block
                              onClick={() => this.notify("bc")}
                            >
                              Bottom Center
                            </Button>
                          </Col>
                          <Col md={4} xs={12}>
                            <Button
                              color="primary"
                              block
                              onClick={() => this.notify("br")}
                            >
                              Bottom Right
                            </Button>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row> */}
      </>
    );
  }
}

export default Notifications;
