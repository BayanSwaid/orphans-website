/*!

=========================================================
* Now UI Dashboard React - v1.4.0
=========================================================

* Product Page: https://www.creative-tim.com/product/now-ui-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/now-ui-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import NotificationAlert from "react-notification-alert";

import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  FormGroup,
  Form,
  Table,
  Input,
  Row,
  Col,
  CardFooter,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
// reactstrap components

// core components
import PanelHeader from "components/PanelHeader/PanelHeader.js";

import { Link } from "react-router-dom";
class RegularTables extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      donors: [],
      donor_name: "",
      modal: false,
    };
  }

  async handleModal(e) {
    e.preventDefault();
    const body = new FormData();
    body.append("name", this.state.donor_name);
    body.append("gender", this.state.gender);
    body.append("phoneNumber", this.state.phoneNumber);
    body.append("email", this.state.email);

    // debugger;
    const response = await fetch(`http://localhost:8001/api/addNewDonor`, {
      method: "POST",
      body,
    });
    const result = await response.json();
    if (result.success) {
      this.setState({
        modal: false,
        donor_name: "",
        gender: "",
        phoneNumber: 0,
        email: "",
      });
      this.notify("info", "Donor");
      this.componentDidMount();
    }
  }
  notify(type, message) {
    var options = {};
    options = {
      place: "tc",
      message: (
        <div>
          <div>
            New <b>{message}</b> was added
          </div>
        </div>
      ),
      type: type,
      icon: "now-ui-icons ui-1_bell-53",
      autoDismiss: 7,
    };
    this.refs.notificationAlert.notificationAlert(options);
  }
  onDismiss() {}
  handleTextEdit = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  async componentDidMount() {
    const resopnse = await fetch(`http://localhost:8001/api/donors`);
    const json = await resopnse.json();
    if (json.success === true) {
      this.setState({
        donors: json["donors"],
      });
    }
  }
  addNewDonor(e) {
    e.preventDefault();
    this.setState({
      modal: true,
    });
  }
  render() {
    return (
      <>
        <NotificationAlert ref="notificationAlert" />

        <Modal isOpen={this.state.modal}>
          <ModalHeader>New Donor</ModalHeader>
          <ModalBody>
            <Row>
              <Col className="pr-1" md="6">
                <FormGroup>
                  <label>Donor Name</label>
                  <Input
                    type="text"
                    name="donor_name"
                    onChange={(e) => {
                      this.handleTextEdit(e);
                    }}
                  />
                </FormGroup>
              </Col>
              <Col className="pr-1" md="6">
                <FormGroup>
                  <label>Gender</label>
                  <Input
                    type="select"
                    name="gender"
                    onChange={(e) => {
                      this.handleTextEdit(e);
                    }}
                  >
                    <option>Choose...</option>
                    <option value="M">Male</option>
                    <option value="F">Female</option>
                  </Input>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col className="pr-1" md="6">
                <FormGroup>
                  <label>Email</label>
                  <Input
                    type="text"
                    name="email"
                    placeholder="example@email.com"
                    onChange={(e) => {
                      this.handleTextEdit(e);
                    }}
                  />
                </FormGroup>
              </Col>
              <Col className="pr-1" md="6">
                <FormGroup>
                  <label>Phone Number</label>
                  <Input
                    type="number"
                    name="phoneNumber"
                    placeholder="012345"
                    onChange={(e) => {
                      this.handleTextEdit(e);
                    }}
                  />
                </FormGroup>
              </Col>
            </Row>
          </ModalBody>
          <ModalFooter>
            <Button
              color="info"
              rounded
              className="btn btn-info btn-sm btn-round"
              onClick={(e) => {
                this.handleModal(e);
              }}
            >
              Add Donor
            </Button>
            <Button
              color="danger"
              rounded
              className="btn btn-danger btn-sm btn-round"
              onClick={(e) => {
                e.preventDefault();
                this.setState({
                  modal: false,
                });
              }}
            >
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
        <PanelHeader size="sm" />
        <div className="content">
          <Row>
            <Col xs={12}>
              <Card>
                <CardHeader>
                  <CardTitle>
                    <h5 className="title">Donors</h5>
                  </CardTitle>
                </CardHeader>
                <CardBody>
                  <Row>
                    {this.state.donors.map((d) => (
                      <Col md="4">
                        <Card className="card-user">
                          <div className="image">
                            <img
                              alt="..."
                              src={require("assets/img/blueBack.png")}
                            />
                          </div>
                          <CardBody>
                            <div className="author">
                                <img
                                  alt="donor avatar avatar"
                                  className="avatar border-gray"
                                  src={
                                    d.gender === "F" || d.gender === "f"
                                      ? require(`assets/img/femaleDonor.png`)
                                      : require(`assets/img/maleDonor.jpg`)
                                  }
                                />
                                <h5
                                  style={{
                                    color: "#00bcd4",
                                    hover:'none'
                                  }}
                                >
                                  {d.name}
                                </h5>
                              <p className="description">{d.phoneNumber}</p>
                            </div>
                            <p className="description text-center">{d.email}</p>
                          </CardBody>
                          <hr />
                          <div className="button-container"></div>
                        </Card>
                      </Col>
                    ))}
                  </Row>
                </CardBody>
                <div className="button-container">
                  <Col className="text-center" md="12">
                    <Button
                      color="info"
                      className="btn btn-info btn-sm btn-round"
                      onClick={(e) => {
                        this.addNewDonor(e);
                      }}
                    >
                      <i class="fa fa-plus" aria-hidden="true"></i> Add New
                      Donor
                    </Button>
                  </Col>
                </div>
              </Card>
            </Col>
            {/* <Col xs={12}>
              <Card className="card-plain">
                <CardHeader>
                  <CardTitle tag="h4">Table on Plain Background</CardTitle>
                  <p className="category"> Here is a subtitle for this table</p>
                </CardHeader>
                <CardBody>
                  <Table responsive>
                    <thead className="text-primary">
                      <tr>
                        {thead.map((prop, key) => {
                          if (key === thead.length - 1)
                            return (
                              <th key={key} className="text-right">
                                {prop}
                              </th>
                            );
                          return <th key={key}>{prop}</th>;
                        })}
                      </tr>
                    </thead>
                    <tbody>
                      {tbody.map((prop, key) => {
                        return (
                          <tr key={key}>
                            {prop.data.map((prop, key) => {
                              if (key === thead.length - 1)
                                return (
                                  <td key={key} className="text-right">
                                    {prop}
                                  </td>
                                );
                              return <td key={key}>{prop}</td>;
                            })}
                          </tr>
                        );
                      })}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col> */}
          </Row>
        </div>
      </>
    );
  }
}

export default RegularTables;
