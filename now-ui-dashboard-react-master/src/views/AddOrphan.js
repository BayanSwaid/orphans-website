import React from "react";
import PanelHeader from "components/PanelHeader/PanelHeader.js";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Row,
  Col,
} from "reactstrap";
import { useHistory } from "react-router-dom";
import NotificationAlert from "react-notification-alert";
export default class AddOrphan extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orphan_nb: -1,
      name: "",
      gender: "",
      father_name: "",
      mother_name: "",
      family_name: "",
      birth_date: 0,
      nationality: "",
      phone_nb: 0,
      family_nb: 0,
      region: "",
      address: "",
      image: "",
      account_nb: 0,
      branch: 0,
      info: "",
    };
  }
  notify(type, message) {
    var options = {};
    options = {
      place: "tc",
      message: (
        <div>
          <div>
            New <b>{message}</b> was added
          </div>
        </div>
      ),
      type: type,
      icon: "now-ui-icons ui-1_bell-53",
      autoDismiss: 7,
    };
    this.refs.notificationAlert.notificationAlert(options);
  }
  onDismiss() {}
  handleTextEdit = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  async addNewOrphan(e) {
    e.preventDefault();
    const body = new FormData();
    body.append("orphan_nb", this.state.orphan_nb);
    body.append("name", this.state.name);
    body.append("father_name", this.state.father_name);
    body.append("mother_name", this.state.mother_name);
    body.append("family_name", this.state.family_name);
    body.append("birth_date", this.state.birth_date);
    body.append("nationality", this.state.nationality);
    body.append("phone_nb", this.state.phone_nb);
    body.append("family_nb", this.state.family_nb);
    body.append("region", this.state.region);
    body.append("address", this.state.address);
    body.append("gender", this.state.gender);
    body.append("image", this.state.image);
    body.append("account_nb", this.state.account_nb);
    body.append("branch", this.state.branch);
    body.append("info", this.state.info);

    // debugger;
    const response = await fetch("http://localhost:8001/api/addNewOrphan", {
      method: "POST",
      body,
    });
    console.log(response);
    const result = await response.json();
    if (result.success) {
      this.notify("info", "Orphan");
      this.setState({
        orphan_nb: -1,
        name: "",
        gender: "",
        father_name: "",
        mother_name: "",
        family_name: "",
        birth_date: 0,
        nationality: "",
        phone_nb: 0,
        family_nb: 0,
        region: "",
        address: "",
        image: "",
        account_nb: 0,
        branch: 0,
        info: "",
      });
    }
  }
  render() {
    return (
      <>
        <NotificationAlert ref="notificationAlert" />

        <PanelHeader size="sm" />
        <div className="content">
          <Row>
            <Col md="12">
              <Card>
                <CardHeader>
                  <h5 className="title">New Orphan</h5>
                </CardHeader>
                <CardBody>
                  <Form>
                    <Row>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Orphan Number</label>
                          <Input
                            type="number"
                            name="orphan_nb"
                            required
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>First Name</label>
                          <Input
                            required
                            name="name"
                            type="text"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Father name</label>
                          <Input
                            type="text"
                            name="father_name"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Family name</label>
                          <Input
                            type="text"
                            name="family_name"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Mother name</label>
                          <Input
                            type="text"
                            name="mother_name"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Birth Date</label>
                          <Input
                            type="number"
                            name="birth_date"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Region</label>
                          <Input
                            name="region"
                            type="text"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Address</label>
                          <Input
                            name="address"
                            type="text"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Branch</label>
                          <Input
                            name="branch"
                            type="number"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="3">
                        <FormGroup>
                          <label>Gender</label>
                          <Input
                            name="gender"
                            type="select"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          >
                            <option>Choose...</option>
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                          </Input>
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="3">
                        <FormGroup>
                          <label>Account Number</label>
                          <Input
                            name="account_nb"
                            type="number"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Family Number</label>
                          <Input
                            name="family_nb"
                            type="number"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Phone Number</label>
                          <Input
                            name="phone_nb"
                            type="number"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Nationality</label>
                          <Input
                            name="nationality"
                            type="text"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="6">
                        <FormGroup>
                          <label>About The orphan</label>
                          <Input
                            cols="80"
                            rows="4"
                            type="textarea"
                            name="info"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="text-center" md="12">
                        <Button
                          color="info"
                          className="btn btn-info btn-sm btn-round"
                          onClick={(e) => {
                            this.addNewOrphan(e);
                          }}
                        >
                          Save
                        </Button>
                      </Col>
                    </Row>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}
