/*!

=========================================================
* Now UI Dashboard React - v1.4.0
=========================================================

* Product Page: https://www.creative-tim.com/product/now-ui-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/now-ui-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import NotificationAlert from "react-notification-alert";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  FormGroup,
  Form,
  Table,
  Input,
  Row,
  Col,
  CardFooter,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
// core components
import PanelHeader from "components/PanelHeader/PanelHeader.js";
import FosterCares from "components/FosterCares/FosterCares";

class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orphan: [],
      members: [],
      modal: false,
      modal1: false,
      relationship: "",
      birth_date_member: -1,
      member_name: "",
      familyNecessities: [],
      necessity_type: "",
      necessity_amount: 0,
      necessity_description: "",
      orphan_nb: -1,
      necessity_date: null,
      name: "",
      gender: "",
      father_name: "",
      mother_name: "",
      family_name: "",
      birth_date: 0,
      nationality: "",
      phone_nb: 0,
      family_nb: 0,
      region: "",
      address: "",
      image: "",
      account_nb: 0,
      branch: 0,
      info: "",
    };
    this.notify = this.notify.bind(this);
    this.onDismiss = this.onDismiss.bind(this);
    this.handleModalNewMember = this.handleModalNewMember.bind(this);
    this.handleModalNewNecessity = this.handleModalNewNecessity.bind(this);
  }
  notify(type, message, end) {
    var options = {};
    options = {
      place: "tc",
      message: (
        <div>
          <div>
            <b>{message}</b>{end}
          </div>
        </div>
      ),
      type: type,
      icon: "now-ui-icons ui-1_bell-53",
      autoDismiss: 7,
    };
    this.refs.notificationAlert.notificationAlert(options);
  }
  onDismiss() {}
  handleTextEdit = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  async getMembers() {
    const response = await fetch(
      `http://localhost:8001/api/familyMember/${this.props.match.params.id}`
    );
    const json = await response.json();
    if (json.success === true) {
      this.setState({
        members: json.member,
      });
    }
  }
  async getOrphan() {
    const response = await fetch(
      `http://localhost:8001/api/orphan/${this.props.match.params.id}`
    );
    const json = await response.json();
    if (json.success === true) {
      this.setState(
        {
          orphan: json.orphan,
        },
        () =>
          this.setState({
            name: this.state.orphan.name,
            gender: this.state.orphan.gender,
            father_name: this.state.orphan.father_name,
            mother_name: this.state.orphan.mother_name,
            family_name: this.state.orphan.family_name,
            birth_date: this.state.orphan.birth_date,
            nationality: this.state.orphan.nationality,
            phone_nb: this.state.orphan.phone_nb,
            family_nb: this.state.orphan.family_nb,
            region: this.state.orphan.region,
            address: this.state.orphan.address,
            image: this.state.orphan.image,
            account_nb: this.state.orphan.account_nb,
            branch: this.state.orphan.branch,
            info: this.state.orphan.info,
          })
      );
    }
  }

  async getNecessities() {
    const response = await fetch(
      `http://localhost:8001/api/necessity/${this.props.match.params.id}`
    );
    const json = await response.json();
    if (json.success === true) {
      this.setState({
        familyNecessities: json.necessities,
      });
    }
  }
  componentDidMount() {
    this.getOrphan();
    this.getMembers();
    this.getNecessities();
  }

  addNewMember = (e) => {
    e.preventDefault();
    this.setState({
      modal: true,
    });
  };
  addNewNecessity = (e) => {
    e.preventDefault();
    this.setState({
      modal1: true,
    });
  };
  async necessityDone(e, id) {
    const body = new FormData();
    body.append("done", e.target.checked ? 1 : 0);
    const response = await fetch(
      `http://localhost:8001/api/editNecessity/${id}`,
      {
        method: "POST",
        body,
      }
    );
    const result = await response.json();
    if (result.success) {
      this.componentDidMount();
      this.notify("info", "Necessity", " was updated")
    }
  }
  handleModalNewMember = async (e) => {
    e.preventDefault();
    const body = new FormData();
    body.append("name", this.state.member_name);
    body.append("brith_date", this.state.birth_date_member);
    body.append("relationship", this.state.relationship);
    body.append("orphan_nb", this.state.orphan.orphan_nb);
    // debugger;
    const response = await fetch(`http://localhost:8001/api/addNewMember`, {
      method: "POST",
      body,
    });
    const result = await response.json();
    if (result.success) {
      this.setState({
        modal: false,
        member_name: "",
        birth_date_member: -1,
        relationship: "",
      });
      this.notify("info", "New Family Member", " was added");
      this.componentDidMount();
    }
  };

  handleModalNewNecessity = async (e) => {
    e.preventDefault();
    const body = new FormData();
    body.append("type", this.state.necessity_type);
    body.append("description", this.state.necessity_description);
    body.append("amount", this.state.necessity_amount);
    body.append("date", this.state.necessity_date);

    body.append("orphan_nb", this.state.orphan.orphan_nb);
    // debugger;
    const response = await fetch(`http://localhost:8001/api/addNewNecessity`, {
      method: "POST",
      body,
    });
    const result = await response.json();
    if (result.success) {
      this.setState({
        modal1: false,
        necessity_amount: 0,
        necessity_description: "",
        necessity_type: "",
      });
      this.notify("info", "New Necessity", " was added");
      this.componentDidMount();
    }
  };
  addNewOrphan(e) {
    e.preventDefault();
  }
  async handleUpdateOrphan() {
    const body = new FormData();
    body.append("name", this.state.name);
    body.append("father_name", this.state.father_name);
    body.append("mother_name", this.state.mother_name);
    body.append("family_name", this.state.family_name);
    body.append("birth_date", this.state.birth_date);
    body.append("nationality", this.state.nationality);
    body.append("phone_nb", this.state.phone_nb);
    body.append("family_nb", this.state.family_nb);
    body.append("region", this.state.region);
    body.append("address", this.state.address);
    body.append("gender", this.state.gender);
    body.append("image", this.state.image);
    body.append("account_nb", this.state.account_nb);
    body.append("branch", this.state.branch);
    body.append("info", this.state.info);
    const response = await fetch(
      `http://localhost:8001/api/editOrphanInfo/${this.props.match.params.id}`,
      {
        method: "POST",
        body,
      }
    );
    const result = await response.json();
    if (result.success) {
      this.notify("info", "Orphan's info", " were updated");
    }
  }
  updateOrphan = (e) => {
    e.preventDefault();
    this.handleUpdateOrphan();
  };
  render() {
    return (
      <>
        <NotificationAlert ref="notificationAlert" />

        <Modal size="lg" isOpen={this.state.modal}>
          <ModalHeader>New Family Member</ModalHeader>
          <ModalBody>
            <Form>
              <Row>
                <Col className="pr-1" md="6">
                  <FormGroup>
                    <label>Orphan Number</label>
                    <Input
                      defaultValue={this.state.orphan.orphan_nb}
                      disabled
                      type="text"
                      {...(this.props.match.params.id < 0 ? "disabled" : null)}
                      name="orphan_nb"
                    />
                  </FormGroup>
                </Col>
                <Col className="pr-1" md="6">
                  <FormGroup>
                    <label>Name</label>
                    <Input
                      name="member_name"
                      type="text"
                      onChange={(e) => {
                        this.handleTextEdit(e);
                      }}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col className="pr-1" md="6">
                  <FormGroup>
                    <label>Birth Date</label>
                    <Input
                      type="number"
                      name="birth_date_member"
                      onChange={(e) => {
                        this.handleTextEdit(e);
                      }}
                    />
                  </FormGroup>
                </Col>
                <Col className="pr-1" md="6">
                  <FormGroup>
                    <label>Relationship</label>
                    <Input
                      name="relationship"
                      type="text"
                      onChange={(e) => {
                        this.handleTextEdit(e);
                      }}
                    />
                  </FormGroup>
                </Col>
              </Row>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button
              color="info"
              rounded
              className="btn btn-info btn-sm btn-round"
              onClick={(e) => {
                this.handleModalNewMember(e);
              }}
            >
              Save
            </Button>
            <Button
              color="danger"
              rounded
              className="btn btn-danger btn-sm btn-round"
              onClick={(e) =>
                this.setState({
                  modal: false,
                })
              }
            >
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
        <Modal size="lg" isOpen={this.state.modal1}>
          <ModalHeader>New Family Necessity</ModalHeader>
          <ModalBody>
            <Form>
              <Row>
                <Col className="pr-1" md="4">
                  <FormGroup>
                    <label>Orphan Number</label>
                    <Input
                      defaultValue={this.state.orphan.orphan_nb}
                      disabled
                      type="text"
                      name="orphan_nb"
                    />
                  </FormGroup>
                </Col>
                <Col className="pr-1" md="4">
                  <FormGroup>
                    <label>Type</label>
                    <Input
                      name="necessity_type"
                      type="text"
                      onChange={(e) => {
                        this.handleTextEdit(e);
                      }}
                    />
                  </FormGroup>
                </Col>
                <Col className="pr-1" md="4">
                  <FormGroup>
                    <label>Date</label>
                    <Input
                      name="necessity_date"
                      type="date"
                      onChange={(e) => {
                        this.handleTextEdit(e);
                      }}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col className="pr-1" md="4">
                  <FormGroup>
                    <label>Amount</label>
                    <Input
                      name="necessity_amount"
                      type="number"
                      onChange={(e) => {
                        this.handleTextEdit(e);
                      }}
                    />
                  </FormGroup>
                </Col>
                <Col className="pr-1" md="8">
                  <FormGroup>
                    <label>Description</label>
                    <Input
                      type="text"
                      name="necessity_description"
                      onChange={(e) => {
                        this.handleTextEdit(e);
                      }}
                    />
                  </FormGroup>
                </Col>
              </Row>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button
              color="info"
              rounded
              className="btn btn-info btn-sm btn-round"
              onClick={(e) => {
                this.handleModalNewNecessity(e);
              }}
            >
              Save
            </Button>
            <Button
              color="danger"
              rounded
              className="btn btn-danger btn-sm btn-round"
              onClick={(e) =>
                this.setState({
                  modal1: false,
                })
              }
            >
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
        <PanelHeader size="sm" />
        <div className="content">
          <Row>
            <Col md="12">
              <Card>
                <CardHeader>
                  <h5 className="title">Orphan Profile</h5>
                </CardHeader>
                <CardBody>
                  <Form>
                    <Row>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Orphan Number</label>
                          <Input
                            defaultValue={this.state.orphan.orphan_nb}
                            disabled
                            type="text"
                            name="orphan_nb"
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>First Name</label>
                          <Input
                            defaultValue={this.state.orphan.name}
                            name="name"
                            type="text"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Father name</label>
                          <Input
                            defaultValue={this.state.orphan.father_name}
                            type="text"
                            name="father_name"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Family name</label>
                          <Input
                            defaultValue={this.state.orphan.family_name}
                            type="text"
                            name="family_name"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Mother name</label>
                          <Input
                            defaultValue={this.state.orphan.mother_name}
                            type="text"
                            name="mother_name"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Birth Date</label>
                          <Input
                            defaultValue={this.state.orphan.birth_date}
                            type="number"
                            name="birth_date"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Region</label>
                          <Input
                            defaultValue={this.state.orphan.region}
                            name="region"
                            type="text"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Address</label>
                          <Input
                            defaultValue={this.state.orphan.address}
                            name="address"
                            type="text"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Branch</label>
                          <Input
                            defaultValue={this.state.orphan.branch}
                            name="branch"
                            type="number"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="3">
                        <FormGroup>
                          <label for="exampleFile">Gender</label>
                          <Input
                            type="select"
                            name="gender"
                            defaultValue={this.state.orphan.gender}
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          >
                            {this.state.orphan.gender == "M" ? (
                              <>
                                <option selected value="M">
                                  Male
                                </option>
                                <option value="F">Female</option>
                              </>
                            ) : (
                              <>
                                <option value="M">Male</option>
                                <option selected value="F">
                                  Female
                                </option>
                              </>
                            )}
                          </Input>
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="3">
                        <FormGroup>
                          <label>Account Number</label>
                          <Input
                            defaultValue={this.state.orphan.account_nb}
                            name="account_nb"
                            type="number"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Family Number</label>
                          <Input
                            defaultValue={this.state.orphan.family_nb}
                            name="family_nb"
                            type="number"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Phone Number</label>
                          <Input
                            defaultValue={this.state.orphan.phone_nb}
                            name="phone_nb"
                            type="number"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="2">
                        <FormGroup>
                          <label>Nationality</label>
                          <Input
                            defaultValue={this.state.orphan.nationality}
                            name="phone_nb"
                            type="text"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="6">
                        <FormGroup>
                          <label>About The orphan</label>
                          <Input
                            defaultValue={this.state.orphan.info}
                            cols="80"
                            rows="4"
                            type="textarea"
                            onChange={(e) => {
                              this.handleTextEdit(e);
                            }}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="text-center" md="12">
                        <Button
                          color="info"
                          className="btn btn-info btn-sm btn-round"
                          onClick={(e) => {
                            this.updateOrphan(e);
                          }}
                        >
                          <i
                            class="now-ui-icons arrows-1_refresh-69"
                            aria-hidden="true"
                          ></i>
                          &nbsp;Update
                        </Button>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-1" md="12">
                        <Card>
                          <CardHeader>
                            <CardTitle tag="h4">Orphan's Family</CardTitle>
                          </CardHeader>
                          <CardBody>
                            <Table responsive>
                              <thead className="text-info">
                                <tr>
                                  <th className="text-center">Name</th>
                                  <th className="text-center">Birth Date</th>
                                  <th className="text-center">Relationship</th>
                                </tr>
                              </thead>
                              <tbody>
                                {this.state.members.map((m) => (
                                  <tr>
                                    <td className="text-center">{m.name}</td>
                                    <td className="text-center">
                                      {m.brith_date}
                                    </td>
                                    <td className="text-center">
                                      {m.relationship}
                                    </td>
                                  </tr>
                                ))}
                              </tbody>
                            </Table>
                          </CardBody>
                          <CardFooter
                            style={{
                              textAlign: "center",
                            }}
                          >
                            <Button
                              color="info"
                              color="info"
                              className="btn btn-info btn-sm btn-round"
                              onClick={(e) => {
                                this.addNewMember(e);
                              }}
                            >
                              <i class="fa fa-plus" aria-hidden="true"></i> Add
                              New Member
                            </Button>
                          </CardFooter>
                        </Card>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-1" md="12">
                        <Card>
                          <CardHeader>
                            <CardTitle tag="h4">Family Necessities</CardTitle>
                          </CardHeader>
                          <CardBody>
                            <Table responsive>
                              <thead className="text-info">
                                <tr>
                                  <th className="text-center">Type</th>
                                  <th className="text-center">Description</th>
                                  <th className="text-center">Amount</th>
                                  <th className="text-center">Date</th>
                                  <th className="text-center">Done</th>
                                </tr>
                              </thead>
                              <tbody>
                                {this.state.familyNecessities.map((n) => (
                                  <tr>
                                    <td className="text-center">{n.type}</td>
                                    <td className="text-center">
                                      {n.description}
                                    </td>
                                    <td className="text-center">{n.amount}</td>
                                    <td className="text-center">{n.date}</td>
                                    <td className="text-center">
                                      <Input
                                        type="checkbox"
                                        defaultChecked={n.done}
                                        onChange={(e) => {
                                          this.necessityDone(e, n.id);
                                        }}
                                      />
                                    </td>
                                  </tr>
                                ))}
                              </tbody>
                            </Table>
                          </CardBody>
                          <CardFooter
                            style={{
                              textAlign: "center",
                            }}
                          >
                            <Button
                              color="info"
                              className="btn btn-info btn-sm btn-round"
                              onClick={this.addNewNecessity}
                            >
                              <i class="fa fa-plus" aria-hidden="true"></i> Add
                              New Necessity
                            </Button>
                          </CardFooter>
                        </Card>
                      </Col>
                    </Row>
                    <FosterCares orphan_nb={this.props.match.params.id} />
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default User;
