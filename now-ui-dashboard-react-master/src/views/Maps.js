/*!

=========================================================
* Now UI Dashboard React - v1.4.0
=========================================================

* Product Page: https://www.creative-tim.com/product/now-ui-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/now-ui-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
// react plugin used to create google maps
// import {
//   withScriptjs,
//   withGoogleMap,
//   GoogleMap,
//   Marker,
// } from "react-google-maps";

// reactstrap components
import { Row, Col, CardTitle, Card, CardHeader, Button, CardBody } from "reactstrap";
import { Link } from "react-router-dom";
// core components
import PanelHeader from "components/PanelHeader/PanelHeader.js";

// const MapWrapper = withScriptjs(
//   withGoogleMap((props) => (
//     <GoogleMap
//       defaultZoom={13}
//       defaultCenter={{ lat: 40.748817, lng: -73.985428 }}
//       defaultOptions={{
//         scrollwheel: false,
//         styles: [
//           {
//             featureType: "water",
//             elementType: "geometry",
//             stylers: [{ color: "#e9e9e9" }, { lightness: 17 }],
//           },
//           {
//             featureType: "landscape",
//             elementType: "geometry",
//             stylers: [{ color: "#f5f5f5" }, { lightness: 20 }],
//           },
//           {
//             featureType: "road.highway",
//             elementType: "geometry.fill",
//             stylers: [{ color: "#ffffff" }, { lightness: 17 }],
//           },
//           {
//             featureType: "road.highway",
//             elementType: "geometry.stroke",
//             stylers: [{ color: "#ffffff" }, { lightness: 29 }, { weight: 0.2 }],
//           },
//           {
//             featureType: "road.arterial",
//             elementType: "geometry",
//             stylers: [{ color: "#ffffff" }, { lightness: 18 }],
//           },
//           {
//             featureType: "road.local",
//             elementType: "geometry",
//             stylers: [{ color: "#ffffff" }, { lightness: 16 }],
//           },
//           {
//             featureType: "poi",
//             elementType: "geometry",
//             stylers: [{ color: "#f5f5f5" }, { lightness: 21 }],
//           },
//           {
//             featureType: "poi.park",
//             elementType: "geometry",
//             stylers: [{ color: "#dedede" }, { lightness: 21 }],
//           },
//           {
//             elementType: "labels.text.stroke",
//             stylers: [
//               { visibility: "on" },
//               { color: "#ffffff" },
//               { lightness: 16 },
//             ],
//           },
//           {
//             elementType: "labels.text.fill",
//             stylers: [
//               { saturation: 36 },
//               { color: "#333333" },
//               { lightness: 40 },
//             ],
//           },
//           { elementType: "labels.icon", stylers: [{ visibility: "off" }] },
//           {
//             featureType: "transit",
//             elementType: "geometry",
//             stylers: [{ color: "#f2f2f2" }, { lightness: 19 }],
//           },
//           {
//             featureType: "administrative",
//             elementType: "geometry.fill",
//             stylers: [{ color: "#fefefe" }, { lightness: 20 }],
//           },
//           {
//             featureType: "administrative",
//             elementType: "geometry.stroke",
//             stylers: [{ color: "#fefefe" }, { lightness: 17 }, { weight: 1.2 }],
//           },
//         ],
//       }}
//     >
//       <Marker position={{ lat: 40.748817, lng: -73.985428 }} />
//     </GoogleMap>
//   ))
// );

class FullScreenMap extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orphans: [],
      allFosterCares: [],
    };
  }
  async componentDidMount() {
    const response2 = await fetch(`http://localhost:8001/api/fosterCares`);
    const json2 = await response2.json();
    if (json2.success === true) {
      this.setState({
        allFosterCares: json2.fosterCares,
      });
    }
    const response = await fetch(`http://localhost:8001/api/orphans`);
    const json = await response.json();
    if (json.success === true) {
      this.setState({
        orphans: json.orphans,
      });
    }
  }
  render() {
    return (
      <>
        <PanelHeader size="sm" />
        <div className="content">
        <Card>
                <CardHeader>
                  <CardTitle><h5 className="title">New Orphans</h5></CardTitle>
                </CardHeader>
                <CardBody>
          <Row>
            {this.state.orphans.map((o) =>
              this.state.allFosterCares.includes(o.orphan_nb) ? null : (
                <Col md="4">
                  <Card className="card-user">
                    <div className="image">
                      <img alt="..." src={require("assets/img/bg4.jpg")} />
                    </div>
                    <CardBody>
                      <div className="author">
                        <Link to={`/admin/orphan-profile/${o.orphan_nb}`}>
                          <img
                            alt="orphan avatar"
                            className="avatar border-gray"
                            src={
                              o.gender === "F" || o.gender === "f"
                                ? require(`assets/img/girl.png`)
                                : require(`assets/img/boy.png`)
                            }
                          />
                          <h5 className="title">{`${o.name} ${o.father_name} ${o.family_name}`}</h5>
                        </Link>
                        <p className="description">{`Orphan Number: ${o.orphan_nb}`}</p>
                      </div>
                      <p className="description text-center">{`Address: ${o.region} - ${o.address}`}</p>
                    </CardBody>
                    <hr />
                    <div className="button-container">
                      <Link to={`/admin/orphan-profile/${o.orphan_nb}`}>
                        <Button
                          color="primary"
                          rounded
                          className="btn btn-primary btn-sm btn-round"
                        >
                          Foster Orphan
                        </Button>
                      </Link>
                    </div>
                  </Card>
                </Col>
              )
            )}
          </Row>
          <Row>
            <Col className="text-center" md="12">
              <Link to="/admin/add-new-orphan">
                <Button
                  color="primary"
                  className="btn btn-primary btn-sm btn-round"
                >
                  <i class="fa fa-plus" aria-hidden="true"></i>
                  &nbsp; &nbsp; Add New Orphan
                </Button>
              </Link>
            </Col>
          </Row>
          </CardBody></Card>
        </div>
      </>
    );
  }
}

export default FullScreenMap;
