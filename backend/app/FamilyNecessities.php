<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FamilyNecessities extends Model
{
    protected $table = 'family_necessities';

    protected $fillable = [
        'type',
        'description',
        'amount',
        'date',
        'orphan_nb',
        'done',
    ];
}
