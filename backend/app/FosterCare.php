<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FosterCare extends Model
{
    protected $table = 'foster_cares';

    protected $fillable = [
        'orphan_nb',
        'donor_id',
        'start_date',
        'end_date',
        'cash_amount',
        'description'
    ];
}
