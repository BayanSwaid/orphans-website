<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orphans extends Model
{
    protected $table = 'orphans';

    protected $fillable = [
        'orphan_nb',
        'name',
        'father_name',
        'mother_name',
        'family_name',
        'birth_date',
        'nationality',
        'phone_nb',
        'family_nb',
        'region',
        'address',
        'image',
        'account_nb',
        'branch',
        'info',
        'gender'
    ];
}
