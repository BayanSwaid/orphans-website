<?php

namespace App\Http\Controllers;

use App\OrphanFamilyMembers;
use Illuminate\Http\Request;

class OrphanFamilyMembersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = OrphanFamilyMembers::all();

        if (!sizeof($result)) {
            return response()->json([
                'success' => false,
                'message' => 'No members were found'
            ], 500);
        }
        return response()->json([
            'success' => true,
            'members' => $result,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        $result = OrphanFamilyMembers::create($inputs);
        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong!'
            ], 500);
        }
        return response()->json([
            'success' => true,
            'message' => 'Member Added'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrphanFamilyMembers  $orphanFamilyMembers
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = OrphanFamilyMembers::where('orphan_nb', $id)->get();
        if (!sizeof($result)) {
            return response()->json([
                'success' => false,
                'message' => 'This member was not found'
            ], 500);
        }
        return response()->json([
            'success' => true,
            'member' => $result
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrphanFamilyMembers  $orphanFamilyMembers
     * @return \Illuminate\Http\Response
     */
    public function edit(OrphanFamilyMembers $orphanFamilyMembers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrphanFamilyMembers  $orphanFamilyMembers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->all();
        $result = OrphanFamilyMembers::where('id', $id)->first();
        $result->update($inputs);

        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong!'
            ], 500);
        }
        return response()->json([
            'success' => true,
            'message' => "Member was updated",
            'member' => $result
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrphanFamilyMembers  $orphanFamilyMembers
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = OrphanFamilyMembers::where('id', $id)->first();
        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'No members were found'
            ], 500);
        }
        $result->delete();

        return response()->json([
            'message' => 'Member was deleted successfully'
        ]);
    }
}
