<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function show($email, $password)
    {
        $result = User::where('email', $email && 'password', $password)->first();
        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'This User was not found'
            ], 500);
        }
        return response()->json([
            'success' => true,
            'user' => $result
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $inputs = $request->all();
        $result = User::where('id', $id)->first();
        $result->update($inputs);

        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong!'
            ], 500);
        }
        return response()->json([
            'success' => true,
            'message' => "Member was updated",
            'member' => $result
        ], 201);
    }
}
