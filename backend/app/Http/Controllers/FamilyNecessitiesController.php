<?php

namespace App\Http\Controllers;

use App\FamilyNecessities;
use Illuminate\Http\Request;

class FamilyNecessitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = FamilyNecessities::all();

        if (!sizeof($result)) {
            return response()->json([
                'success' => false,
                'message' => 'No necessities were found'
            ], 500);
        }

        return response()->json([
            'success' => true,
            'necessities' => $result,
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**orphanFamilyMembers
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        $result = FamilyNecessities::create($inputs);
        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong!'
            ], 500);
        }
        return response()->json([
            'success' => true,
            'message' => 'Necessity Added'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FamilyNecessities  $familyNecessities
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = FamilyNecessities::where('orphan_nb', $id)->get();

        if (!sizeof($result)) {
            return response()->json([
                'success' => false,
                'message' => 'This necessity was not found'
            ], 500);
        }

        return response()->json([
            'success' => true,
            'necessities' => $result
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FamilyNecessities  $familyNecessities
     * @return \Illuminate\Http\Response
     */
    public function edit(FamilyNecessities $familyNecessities)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FamilyNecessities  $familyNecessities
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->all();
        $result = FamilyNecessities::where('id', $id)->first();
        $result->update($inputs);

        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong!'
            ], 500);
        }
        return response()->json([
            'success' => true,
            'message' => "Necessity was updated",
            'necessity' => $result
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FamilyNecessities  $familyNecessities
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = FamilyNecessities::where('id', $id)->first();
        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'No necessities found'
            ], 500);
        }
        $result->delete();

        return response()->json([
            'message' => 'Necessity was deleted successfully'
        ]);
    }
}
