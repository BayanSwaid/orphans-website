<?php

namespace App\Http\Controllers;

use App\Donors;
use Illuminate\Http\Request;

class DonorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Donors::all();

        if (!sizeof($result)) {
            return response()->json([
                'success' => false,
                'message' => 'No donors were found'
            ], 500);
        }

        return response()->json([
            'success' => true,
            'donors' => $result,
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        $result = Donors::create($inputs);
        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong!'
            ], 500);
        }
        return response()->json([
            'success' => true,
            'message' => 'Donor was Added'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Donors  $donors
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = Donors::where('id', $id)->pluck('name')->first();

        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'This donor was not found'
            ], 500);
        }

        return response()->json([
            'success' => true,
            'donor' => $result
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Donors  $donors
     * @return \Illuminate\Http\Response
     */
    public function edit(Donors $donors)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Donors  $donors
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->all();
        $result = Donors::where('id', $id)->first();
        $result->update($inputs);

        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong!'
            ], 500);
        }
        return response()->json([
            'success' => true,
            'message' => "Donor info were updated",
            'donor' => $result
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Donors  $donors
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $result = Donors::where('id', $id)->first();
        // if (!$result) {
        //     return response()->json([
        //         'success' => false,
        //         'message' => 'No donors were found'
        //     ], 500);
        // }
        // $result->delete();

        // return response()->json([
        //     'message' => 'Donor was deleted successfully'
        // ]);
    }
}
