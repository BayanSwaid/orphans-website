<?php

namespace App\Http\Controllers;

use App\FosterCare;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FosterCareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = DB::table('foster_cares')->pluck('orphan_nb');

        if (!sizeof($result)) {
            return response()->json([
                'success' => false,
                'message' => 'No foster cares were found'
            ], 500);
        }

        return response()->json([
            'success' => true,
            'fosterCares' => $result,
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        $result = FosterCare::create($inputs);
        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong!'
            ], 500);
        }
        return response()->json([
            'success' => true,
            'message' => 'Foster care Added'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FosterCare  $fosterCare
     * @return \Illuminate\Http\Response
     */
    public function showPresentOrphans()
    {
        $result = DB::table('foster_cares')->where('end_date','>=', date("Y"))->pluck('orphan_nb');
        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'This foster care was not found'
            ], 500);
        }
        return response()->json([
            'success' => true,
            'presentOrphans' => $result
        ], 200);
    }

    public function showStoppedOrphans()
    {
        $result = DB::table('foster_cares')->where('end_date', '<', date("Y"))->pluck('orphan_nb');
        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'Foster care were not found'
            ], 500);
        }
        return response()->json([
            'success' => true,
            'stoppedOrphans' => $result
        ], 200);
    }

    public function show($id)
    {
        $result = FosterCare::where('orphan_nb', $id)->get();
        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'This foster care was not found'
            ], 500);
        }
        return response()->json([
            'success' => true,
            'fosterCares' => $result
        ], 200);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FosterCare  $fosterCare
     * @return \Illuminate\Http\Response
     */
    public function edit(FosterCare $fosterCare)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FosterCare  $fosterCare
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->all();
        $result = FosterCare::where('id', $id)->first();
        $result->update($inputs);

        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong!'
            ], 500);
        }
        return response()->json([
            'success' => true,
            'message' => "Member was updated",
            'foster-care' => $result
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FosterCare  $fosterCare
     * @return \Illuminate\Http\Response
     */
    public function destroy(FosterCare $fosterCare)
    {
        //
    }
}
