<?php

namespace App\Http\Controllers;

use App\FamilyNecessities;
use App\Orphans;
use Illuminate\Http\Request;

class OrphansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Orphans::all();

        if (!sizeof($result)) {
            return response()->json([
                'success' => false,
                'message' => 'No orphans found'
            ], 500);
        }

        return response()->json([
            'success' => true,
            'orphans' => $result,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();

        // if ($inputs['image']) {
        //     $orphan = new Orphans();
        //     $orphan->fill($inputs);

        //     // Get filename with ext
        //     $filenameWithExt = $inputs['image']->getClientOriginalName();

        //     // Get just filename
        //     $filename = pathinfo($filenameWithExt);

        //     // Get just extension
        //     $extension = $inputs['image']->getClientOriginalExtension();

        //     // Filename to store
        //     $fileNameToStore = $filename['filename'] . '' . time() . '.' . $extension;

        //     // Upload image
        //     $folder = public_path('/assets/img');

        //     if (!file_exists($folder)) {
        //         if (!mkdir($folder, 0777, true) && !is_dir($folder)) {
        //             throw new \RuntimeException(sprintf('Directory "%s" was not created', $folder));
        //         }
        //     }

        //     move_uploaded_file($inputs['image'], $folder . '/' . $fileNameToStore);

        //     $orphan->image = $folder . '/' . $fileNameToStore;
        //     $orphan->save();

        //     return response()->json([
        //         'success' => true,
        //         'message' => 'Orphan Added'
        //     ], 200);
        // } else {
        $orphan = new Orphans();
        $orphan->fill($inputs);
        $orphan->save();
        return response()->json([
            'success' => true,
            'msg' => 'added'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Orphans  $orphans
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = Orphans::where('orphan_nb', $id)->first();

        if (!($result)) {
            return response()->json([
                'success' => false,
                'message' => 'This orphan was not found'
            ], 500);
        }

        return response()->json([
            'success' => true,
            'orphan' => $result
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Orphans  $orphans
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $inputs = $request->all();
        $result = Orphans::where('orphan_nb', $id);
        $result->update($inputs);

        if (!$result) {
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong!'
            ], 500);
        }
        return response()->json([
            'success' => true,
            'message' => "orphan's info were updated",

        ], 200);


        // } else {
        //     if ($inputs['image']) {


        //         // Get filename with ext
        //         $filenameWithExt = $inputs['image']->getClientOriginalName();

        //         // Get just filename
        //         $filename = pathinfo($filenameWithExt);

        //         // Get just extension
        //         $extension = $inputs['image']->getClientOriginalExtension();

        //         // Filename to store
        //         $fileNameToStore = $filename['filename'] . '' . time() . '.' . $extension;

        //         // Upload image
        //         $folder = public_path('/assets/img');

        //         if (!file_exists($folder)) {
        //             if (!mkdir($folder, 0777, true) && !is_dir($folder)) {
        //                 throw new \RuntimeException(sprintf('Directory "%s" was not created', $folder));
        //             }
        //         }

        //         move_uploaded_file($inputs['image'], $folder . '/' . $fileNameToStore);
        //         $inputs['image'] = $folder . '/' . $fileNameToStore;
        //     }

    }
}
