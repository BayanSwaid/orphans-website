<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrphanFamilyMembers extends Model
{
    protected $table = 'orphan_family_members';

    protected $fillable = [
        'name',
        'brith_date',
        'relationship',
        'orphan_nb'
    ];
}
