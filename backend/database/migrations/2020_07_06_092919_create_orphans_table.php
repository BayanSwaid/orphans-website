<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrphansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orphans', function (Blueprint $table) {
            $table->integer('orphan_nb');
            $table->primary('orphan_nb');
            $table->string('name');
            $table->char('gender', 1)->nullable();
            $table->string('father_name')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('family_name')->nullable();
            $table->integer('birth_date')->nullable();
            $table->string('nationality')->nullable();
            $table->string('phone_nb')->nullable();
            $table->integer('family_nb')->nullable();
            $table->string('region')->nullable();
            $table->string('address')->nullable();
            $table->string('image')->nullable();
            $table->string('account_nb')->nullable();
            $table->integer('branch')->nullable();
            $table->string('info')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orphans');
    }
}
