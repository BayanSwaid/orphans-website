<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFamilyNecessitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family_necessities', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->string('type');
            $table->string('description')->nullable();
            $table->float('amount')->nullable();
            $table->date('date')->nullable();
            $table->boolean('done')->default(false);
            $table->integer('orphan_nb');
            $table->foreign('orphan_nb')->references('orphan_nb')->on('orphans');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_necessities');
    }
}
