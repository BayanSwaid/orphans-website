<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrphanFamilyMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orphan_family_members', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->string('name');
            $table->integer('brith_date')->nullable();
            $table->string('relationship')->nullable();
            $table->integer('orphan_nb');
            $table->foreign('orphan_nb')->references('orphan_nb')->on('orphans');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orphan_family_members');
    }
}
