<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFosterCaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foster_cares', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->integer('orphan_nb');            
            $table->bigInteger('donor_id')->unsigned();            
            $table->integer('start_date');
            $table->integer('end_date')->nullable();
            $table->float('cash_amount')->nullable();
            $table->string('description')->nullable();
            $table->foreign('orphan_nb')->references('orphan_nb')->on('orphans');
            $table->foreign('donor_id')->references('id')->on('donors');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foster_cares');
    }
}
