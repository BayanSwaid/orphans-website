<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'AuthController@login');
Route::post('/logout', 'AuthController@logout');
Route::post('/register', 'AuthController@register');

Route::group(['middleware' => ['jwt.verify']], function() {

});
Route::get('/orphans', 'OrphansController@index');
Route::get('/orphan/{id}', 'OrphansController@show');
Route::post('/addNewOrphan', 'OrphansController@store');
Route::post('/editOrphanInfo/{id}', 'OrphansController@update');

Route::get('/necessities', 'FamilyNecessitiesController@index');
Route::get('/necessity/{id}', 'FamilyNecessitiesController@show');
Route::post('/addNewNecessity', 'FamilyNecessitiesController@store');
Route::post('/editNecessity/{id}', 'FamilyNecessitiesController@update');
Route::delete('/deleteNecessity/{id}', 'FamilyNecessitiesController@destroy');

Route::get('/donors', 'DonorsController@index');
Route::get('/donor/{id}', 'DonorsController@show');
Route::post('/addNewDonor', 'DonorsController@store');
Route::post('/editDonorInfo/{id}', 'DonorsController@update');

Route::get('/familyMembers', 'OrphanFamilyMembersController@index');
Route::get('/familyMember/{id}', 'OrphanFamilyMembersController@show');
Route::post('/addNewMember', 'OrphanFamilyMembersController@store');
Route::post('/editMemberInfo/{id}', 'OrphanFamilyMembersController@update');
Route::delete('/deleteMember/{id}', 'OrphanFamilyMembersController@destroy');

Route::get('/fosterCares', 'FosterCareController@index');
Route::get('/fosterCare/{id}', 'FosterCareController@show');
Route::get('/fosterCarePresentOrphans', 'FosterCareController@showPresentOrphans');
Route::post('/addNewFosterCare', 'FosterCareController@store');
Route::post('/editFosterCare/{id}', 'FosterCareController@update');
Route::get('/stoppedOrphans', 'FosterCareController@showStoppedOrphans');


